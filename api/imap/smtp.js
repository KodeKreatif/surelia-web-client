/**
 * Class to handle SMTP connection
 * @param {String} ip - The ip address of the server
 */
var SMTP = function(ip) {
}

/**
 * Authenticates to the SMTP server
 * @param {String} username - Username
 * @param {String} password - Password
 */
SMTP.prototype.auth = function(username, password) {
}

/**
 * Send message to the server
 * @param {String[]} recipients - The recipients of the email
 * @param {String} data - The email message
 */
SMTP.prototype.send = function(recipients, data) {
}

